# ASCII with YL-3 display (hc595 x 2) #
(cc) Heli Tejedor, December 2013, http://heli.xbot.es, helitp@arrakis.es

(cc) thesbb, December 2015, http://bitbucket.org/thesbb


License: Creative Commons 3.0 Attribution-NonCommercial-ShareAlike 3.0 (CC BY-NC-SA 3.0)

http://creativecommons.org/licenses/by-nc-sa/3.0/


## Library for Arduino to control 8 seven-segment displays via ShiftRegister74HC595 on a YL3-board. ##
Dependencies: ShiftRegister74HC595 from http://shiftregister.simsso.de/