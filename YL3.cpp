/*
 * ASCII with YL-3 display (hc595 x 2)
 * (cc) Heli Tejedor, December 2013, http://heli.xbot.es, helitp@arrakis.es
 * (cc) thesbb, December 2015, http://bitbucket.org/thesbb
 *
 * License Creative Commons 3.0 Attribution-NonCommercial-ShareAlike 3.0 (CC BY-NC-SA 3.0)
 * http://creativecommons.org/licenses/by-nc-sa/3.0/
*/

// For different wiring diagram change #define _a to _h  
// with the new bit pattern                              

#if ARDUINO >= 100
#include <Arduino.h>
#else
#include <WProgram.h>
#endif

#include "../ShiftRegister74HC595/ShiftRegister74HC595.h"
#include "../ShiftRegister74HC595/ShiftRegister74HC595.cpp"
#include <RBD_Timer.h>
#include "YL3.h"
#define _a 0x01          //         ------           
#define _b 0x02          //     (f)| (a)  |(b)       
#define _c 0x04          //        |      |          
#define _d 0x08          //         ------           
#define _e 0x10          //     (e)| (g)  |(c)       
#define _f 0x20          //        |      |          
#define _g 0x40          //         -----  o (h)     
#define _h 0x80          //          (d)             

//                 _h|_g|_f|_e|_d|_c|_b|_a           

#define LEDS_BLANK 0   

#define DP_BIT     7                         // Bit number of _h, decimal point
#define CURSOR_BIT 3                         // Bit number of _d, cursor
#define LEDS_DP    _h                        // OR with other LEDS_ 
#define LEDS_CURSOR            _d                 

#define LEDS_High                       _a   //  , bit 0     
#define LEDS_Low               _d            // _, bit 3         
#define LEDS_Float    _g                     // -, bit 6 

#define LEDS_0           _f|_e|_d|_c|_b|_a
#define LEDS_1                    _c|_b       
#define LEDS_2        _g   |_e|_d|   _b|_a
#define LEDS_3        _g      |_d|_c|_b|_a
#define LEDS_4        _g|_f      |_c|_b
#define LEDS_5        _g|_f   |_d|_c   |_a 
#define LEDS_6        _g|_f|_e|_d|_c   |_a                   
#define LEDS_7                    _c|_b|_a          
#define LEDS_8        _g|_f|_e|_d|_c|_b|_a 
#define LEDS_9        _g|_f      |_c|_b|_a    

#define LEDS_A        _g|_f|_e   |_c|_b|_a                  
#define LEDS_B        _g|_f|_e|_d|_c                        
#define LEDS_C           _f|_e|_d      |_a           
#define LEDS_Cl       _g   |_e|_d             
#define LEDS_D        _g   |_e|_d|_c|_b                  
#define LEDS_E        _g|_f|_e|_d      |_a                
#define LEDS_F        _g|_f|_e         |_a             
#define LEDS_G           _f|_e|_d|_c   |_a                
#define LEDS_H        _g|_f|_e   |_c|_b
#define LEDS_Hl       _g|_f|_e   |_c
#define LEDS_I                    _c|_b          
#define LEDS_Il                   _c 
#define LEDS_J              _e|_d|_c|_b                
#define LEDS_K        _g|_f|_e                      
#define LEDS_L           _f|_e|_d                   
#define LEDS_M           _f|_e   |_c|_b|_a              
#define LEDS_N        _g   |_e   |_c                     
#define LEDS_O           _f|_e|_d|_c|_b|_a      
#define LEDS_Ol       _g   |_e|_d|_c                       
#define LEDS_P        _g|_f|_e      |_b|_a                 
#define LEDS_Q        _g|_f      |_c|_b|_a                
#define LEDS_R        _g   |_e                   
#define LEDS_S        _g|_f   |_d|_c   |_a                 
#define LEDS_T        _g|_f|_e|_d                      
#define LEDS_U           _f|_e|_d|_c|_b           
#define LEDS_Ul             _e|_d|_c              
#define LEDS_V              _e|_d|_c                 
#define LEDS_W        _f|_e|_d|_c|_b
#define LEDS_X        _g|_f      |_c|_b                                      
#define LEDS_Y        _g|_f   |_d|_c|_b     
#define LEDS_Z        _g   |_e|_d   |_b|_a    

#define LEDS_ENIE     _g   |_e   |_c   |_a        //ñ

#define LEDS_OC          _f|_e|_d      |_a
#define LEDS_CC                _d|_c|_b|_a
#define LEDS_SLASH    _g   |_e      |_b
#define LEDS_UPPERo   _g|_f         |_b|_a   
#define LEDS_EQUAL    _g      |_d         


RBD::Timer tickerTimer;

// Periodic refresh 
void YL3::UpdateDisplay(void)
{
  static int DigitCnt=0;
  //static int Digits=0x01; YL3 is this way...
  static int Digits=0x80; 

  Send16 (MsgDisplay[DigitCnt], Digits);

  Digits>>=1;  
  if (++DigitCnt>=NDIGITS) {DigitCnt=0; Digits=0x80;}

  if (tickerTimer.onRestart())
    UpdateTicker();
}

void YL3::Send16 (byte Segments, byte Digits)
{
  ShiftRegister74HC595 sr(2, _pin_dio, _pin_sck, _pin_rck);
  uint8_t pinValues[2];

  pinValues[0] = Segments^0xFF;
  pinValues[1] = Digits;
  sr.setAll(pinValues);
}

void YL3::DisplayChar (char Ch)
{
  // a to z lowercase letters pattern
  const byte LettersL[26] = {
    LEDS_A,  LEDS_B,  LEDS_Cl, LEDS_D,  LEDS_E,  LEDS_F,  LEDS_G,  LEDS_Hl,
    LEDS_Il, LEDS_J,  LEDS_K,  LEDS_L,  LEDS_M,  LEDS_N,  LEDS_Ol, LEDS_P,
    LEDS_Q,  LEDS_R,  LEDS_S,  LEDS_T,  LEDS_Ul, LEDS_V,  LEDS_W,  LEDS_X,
    LEDS_Y,  LEDS_Z};

  // A to Z uppercase letters pattern
  const byte LettersU[26] = {
    LEDS_A,  LEDS_B,  LEDS_C,  LEDS_D,  LEDS_E,  LEDS_F,  LEDS_G,  LEDS_H,
    LEDS_I,  LEDS_J,  LEDS_K,  LEDS_L,  LEDS_M,  LEDS_N,  LEDS_O,  LEDS_P, 
    LEDS_Q,  LEDS_R,  LEDS_S,  LEDS_T,  LEDS_U,  LEDS_V,  LEDS_W,  LEDS_X, 
    LEDS_Y,  LEDS_Z};

  // 0 to 9 numbers pattern
  const byte Numbers[10] = {
    LEDS_0, LEDS_1, LEDS_2, LEDS_3, LEDS_4, LEDS_5, LEDS_6, LEDS_7, LEDS_8, LEDS_9};

  // ASCII Numbers and letters to seven segments 
  if (Ch>='0' && Ch<='9') MsgDisplay[BuffPtr]=Numbers[Ch-'0'];
  else if (Ch>='a' && Ch<='z') MsgDisplay[BuffPtr]=LettersL[Ch-'a'];
  else if (Ch>='A' && Ch<='Z') MsgDisplay[BuffPtr]=LettersU[Ch-'A'];    
  // (Spanish) Ñ or ñ
  else if (Ch==209 || Ch==241) MsgDisplay[BuffPtr]=LEDS_ENIE; 
  // ASCII special chars
  else if (Ch==' ') MsgDisplay[BuffPtr]=LEDS_BLANK;
  else if (Ch=='-') MsgDisplay[BuffPtr]=LEDS_Float;
  else if (Ch=='_') MsgDisplay[BuffPtr]=LEDS_Low;
  else if (Ch=='/') MsgDisplay[BuffPtr]=LEDS_SLASH;    
  else if (Ch=='=') MsgDisplay[BuffPtr]=LEDS_EQUAL;    
  else if (Ch==186) MsgDisplay[BuffPtr]=LEDS_UPPERo;    
  // ' 
  else if (Ch==39) MsgDisplay[BuffPtr]=_f; 
  // " 
  else if (Ch==34) MsgDisplay[BuffPtr]=_b|_f;    
  // Decimal point
  else if (Ch=='.' ||Ch==',')
  {
    if (BuffPtr<=0) BuffPtr=NDIGITS; 
    MsgDisplay[--BuffPtr]|=LEDS_DP;    
  }
  // Backspace ^H
  else if (Ch==8)
  {
    if (BuffPtr--<=0) BuffPtr=0;
    return;
  }
  // Carriage return ^M
  else if (Ch==13){ BuffPtr=0; return; }
  // Delete 
  else if (Ch==127)
  {
    if (BuffPtr<=0) BuffPtr=NDIGITS; 
    MsgDisplay[--BuffPtr]=LEDS_BLANK;
    return;
  }
  // Clear display (New line) ^J 
  else if (Ch==10)
  {
    for (BuffPtr=0; BuffPtr<=NDIGITS; BuffPtr++)
      MsgDisplay[BuffPtr]=LEDS_BLANK;
    BuffPtr=0; 
    return;
  }
  // Non valid char, return without chage BuffPtr   
  else return;

  // Increment and wrap buffer pointer
  if (++BuffPtr>=NDIGITS) BuffPtr=0;
}

void YL3::Ticker(char text[MAXTICKER] = " ")
{
  size_t destination_size = sizeof (_tickerText);
  strncpy(_tickerText, text, destination_size);
  _tickerText[destination_size - 1] = '\0';
}

void YL3::UpdateTicker(void)
{
  static int pos = NDIGITS;
  static int fertig = NDIGITS + MAXTICKER;
  int i;
  char displayText[9];
  tickerDone = false;
  for(i = 0; i <= NDIGITS; i++)
  {
    if(i < pos) displayText[i] = ' ';
    if(i >=pos) displayText[i] = _tickerText[i-pos];
  }

  if(pos == 0)
  {
    for(i = 1; i <= MAXTICKER-1; i++)
    {
      if(_tickerText[i] == '\0')
        _tickerText[i-1] = ' ';
      else
        _tickerText[i-1] = _tickerText[i];
    }
  }
  if(pos > 0) pos--;
  DisplayString(displayText);
  fertig--;
  if(fertig < 0)
  {
    tickerTimer.stop();
    tickerDone = true;
    pos = NDIGITS;
    fertig = NDIGITS + MAXTICKER;
  }
}

void YL3::StartTicker(void)
{
  tickerDone = false;
  tickerTimer.restart();
}

void YL3::DisplayString(char text[9] = "        ")
{
  DisplayChar(text[0]);
  DisplayChar(text[1]);
  DisplayChar(text[2]);
  DisplayChar(text[3]);
  DisplayChar(text[4]);
  DisplayChar(text[5]);
  DisplayChar(text[6]);
  DisplayChar(text[7]);
}

// Constructor     
YL3::YL3(int pin_dio, int pin_sck, int pin_rck)
{
  _pin_dio = pin_dio;
  _pin_sck = pin_sck;
  _pin_rck = pin_rck; 

  BuffPtr=0; 

  tickerTimer.setTimeout(300);
  tickerTimer.stop();
  tickerDone = true;
}
