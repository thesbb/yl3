/*
 * ASCII with YL-3 display (hc595 x 2)
 * (cc) Heli Tejedor, December 2013, http://heli.xbot.es, helitp@arrakis.es
 * (cc) thesbb, December 2015, http://bitbucket.org/thesbb
 *
 * License Creative Commons 3.0 Attribution-NonCommercial-ShareAlike 3.0 (CC BY-NC-SA 3.0)
 * http://creativecommons.org/licenses/by-nc-sa/3.0/
*/
#ifndef YL3_H
#define YL3_H

#if ARDUINO >= 100
#include <Arduino.h>
#else
#include <WProgram.h>
#endif

// Display digits (8)
#define NDIGITS 8

// Max ticker text length
#define MAXTICKER 30

class YL3
{
  public:
    YL3(int pin_dio, int pin_sck, int pin_rck);
    void DisplayChar (char Ch);
    void UpdateDisplay(void);
    void Ticker(char text[17]);
    void StartTicker(void);
    bool tickerDone;
  private:
    void Send16 (byte Segments, byte Digits);    
    void UpdateTicker(void);
    void DisplayString(char text[9]);
    // Write pointer to buffer  
    int BuffPtr;
    int _pin_dio;
    int _pin_sck;
    int _pin_rck;
    char _tickerText[MAXTICKER];
    // Display Buffer 
    byte MsgDisplay[NDIGITS];
};
#endif
