#include <YL3.h>
#include <RBD_Timer.h>

RBD::Timer updateTimer;
RBD::Timer nextTimer;

// Display initialisieren
YL3 Display(7, 6, 5);


void setup() {
  updateTimer.setTimeout(1);
  updateTimer.restart();
  nextTimer.setTimeout(1000);
  nextTimer.restart();

  Display.Ticker("SF SOFTWARE SOLUTIONS");
  Display.StartTicker();
}

void loop() {
  bool anzeigeAcht = false;
  if (updateTimer.onRestart())
  {
    Display.UpdateDisplay();
  }
  if (nextTimer.onRestart())
  {
    if (Display.tickerDone)
    {
      if (!anzeigeAcht)
      {
        Display.DisplayChar('8');
        Display.DisplayChar('.');
        Display.DisplayChar('8');
        Display.DisplayChar('.');
        Display.DisplayChar('8');
        Display.DisplayChar('.');
        Display.DisplayChar('8');
        Display.DisplayChar('.');
        Display.DisplayChar('8');
        Display.DisplayChar('.');
        Display.DisplayChar('8');
        Display.DisplayChar('.');
        Display.DisplayChar('8');
        Display.DisplayChar('.');
        Display.DisplayChar('8');
        Display.DisplayChar('.');
        anzeigeAcht = true;
      }
    }
  }
}
